<?php

/**
 * This is the model class for table "tbl_task".
 *
 * The followings are the available columns in table 'tbl_task':
 * @property integer $task_id
 * @property integer $id_creator
 * @property string $date_task
 * @property string $title
 * @property string $info
 * @property integer $price
 * @property integer $pay_type
 * @property integer $cash_type
 * @property integer $spec
 * @property integer $profy
 * @property integer $verify
 * @property integer $pay_method
 */
class Task extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_creator, price, pay_type, cash_type, spec, profy, verify, pay_method', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>128),
			array('date_task, info', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('task_id, id_creator, date_task, title, info, price, pay_type, cash_type, spec, profy, verify, pay_method', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'task_id' => 'Task',
			'id_creator' => 'Id Creator',
			'date_task' => 'Date Task',
			'title' => 'Title',
			'info' => 'Info',
			'price' => 'Price',
			'pay_type' => 'Pay Type',
			'cash_type' => 'Cash Type',
			'spec' => 'Spec',
			'profy' => 'Profy',
			'verify' => 'Verify',
			'pay_method' => 'Pay Method',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('id_creator',$this->id_creator);
		$criteria->compare('date_task',$this->date_task,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('pay_type',$this->pay_type);
		$criteria->compare('cash_type',$this->cash_type);
		$criteria->compare('spec',$this->spec);
		$criteria->compare('profy',$this->profy);
		$criteria->compare('verify',$this->verify);
		$criteria->compare('pay_method',$this->pay_method);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
