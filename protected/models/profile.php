<?php

/**
 * This is the model class for table "tbl_usercard".
 *
 * The followings are the available columns in table 'tbl_usercard':
 * @property integer $card_id
 * @property string $lico
 * @property integer $user_id
 * @property string $name
 * @property string $family
 * @property string $avatar
 * @property string $phone
 * @property string $skype
 * @property string $site
 * @property string $special
 * @property string $showemail
 * @property string $country
 * @property string $city
 */
class profile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	 
	 
	 
	public $icon; // атрибут для хранения загружаемой картинки статьи
	public $del_img; // атрибут для удаления уже загруженной карти
	
	public function tableName()
	{
		return 'tbl_usercard';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('del_img', 'boolean'),
		
		
			array('user_id, name, family', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('lico, showemail', 'length', 'max'=>1),
			array('name, family, avatar, skype, site', 'length', 'max'=>128),
			array('phone', 'length', 'max'=>20),
			array('special', 'length', 'max'=>100),
			array('country, city', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('card_id, lico, user_id, name, family, avatar, phone, skype, site, special, showemail, country, city', 'safe', 'on'=>'search'),
		
		
		
		
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'card_id' => 'Card',
			'lico' => 'Lico',
			'user_id' => 'User',
			'name' => 'Name',
			'family' => 'Family',
			'avatar' => 'Avatar',
			'phone' => 'Phone',
			'skype' => 'Skype',
			'site' => 'Site',
			'special' => 'Special',
			'showemail' => 'Showemail',
			'country' => 'Country',
			'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('card_id',$this->card_id);
		$criteria->compare('lico',$this->lico,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('family',$this->family,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('site',$this->site,true);
		$criteria->compare('special',$this->special,true);
		$criteria->compare('showemail',$this->showemail,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('city',$this->city,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
