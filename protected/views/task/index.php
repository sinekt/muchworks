<?php
/* @var $this TaskController */
/* @var $dataProvider CActiveDataProvider */
$this->layout = '//layouts/main';
?>

   <section id="project">
        <div class="container">


<?php
$task=Task::model()->findByPk(1);


$tasks = Task::model()->findAll();


?>

<?php foreach($tasks as $task) : ?>






               <div class="col-md-6">
                   <div class="project border-top-green">
                       <a href="<?php echo Yii::app()->request->baseUrl; echo 'task/view/id/'; echo $task->task_id; ?>" class="nodecoration">
                        <p>
                            <span class="color-grey">Добавил: </span>
                            <span class="color-green"><?php $creator_id=$task->id_creator; $creator=User::model()->findByPk($creator_id); echo $creator->username; ?></span>
                            <span class="color-grey"><?php echo $task->date_task; ?></span>
                        </p>
                        <h4><?php echo $task->title; ?></h4>
                        <p class="color-grey">
						
						<?php $profy=$task->profy; if($profy=='1') { ?>
                            <span class="only">Только для
                                <img src="design/profi.png" alt="" class="img-responsive">
                            </span>
						<?php } ?>
							
						<?php $verify=$task->verify; if($verify=='1') { ?>	
                            <span class="only">Только для
                                <span class="glyphicon glyphicon-user"></span>    
                            </span>
						<?php } ?>
						
						<?php $cash_type=$task->cash_type; if($cash_type=='1') { ?>	
                            <span class="only">Безопасная сделка
                                <span class="glyphicon glyphicon-check"></span>    
                            </span>
						<?php } ?>
                        </p>
                            <hr>
                            <span class="price">
                                <span class="color-grey">Посмотреть проект <span class="glyphicon glyphicon-arrow-right" style="opacity:0.5;"></span></span>
                                <span class="color-red pull-right"><?php echo $task->price; ?> ₽ / <?php echo $task->pay_type; ?></span>
                            </span>          
                        </a>              
                    </div>
                </div>
				
<?php endforeach ?>


        </div>
    </section>
	
