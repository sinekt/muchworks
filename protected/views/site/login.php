<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
$this->layout = '//layouts/black';
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="row top-links">
                <div class="col-md-12">
                    <p class="text-center"><a href="/index.php/user/create/">Регистрация</a>&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp; Вход на сайт</p>
                </div>
</div>	

<div class="row">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>


	<div class="row">
	
		<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>128, 'class'=>'auth-inp', 'placeholder'=>'Логин')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
	
		<?php echo $form->passwordField($model,'password',array('size'=>20,'maxlength'=>128, 'class'=>'auth-inp', 'placeholder'=>'Пароль')); ?>
		<?php echo $form->error($model,'password'); ?>

		
		
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'Запомнить меня',array('class'=>'gray')); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

 	<div class="row buttons otstup">
		<?php echo CHtml::submitButton('Войти на сайт', array( 'class'=>'in-button')); ?>
		<a href="#" class="restore">Забыли?</a>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
