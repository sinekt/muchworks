<?php
/* @var $this WorkController */
/* @var $data Work */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->work_id), array('view', 'id'=>$data->work_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_creator')); ?>:</b>
	<?php echo CHtml::encode($data->id_creator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_work')); ?>:</b>
	<?php echo CHtml::encode($data->date_work); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info')); ?>:</b>
	<?php echo CHtml::encode($data->info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />


</div>