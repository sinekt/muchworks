<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>
<style>
.clean {padding-top:5%; height:auto;}
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
	<?php 
	
	$id_creator=Yii::app()->user->id; 
	$data=date("Y-m-d H:i:s");
	?>
	

		<?php echo $form->textField($model,'id_creator',array('class'=>'none', 'value'=>$id_creator)); ?>

	</div>

	<div class="row">
	
		<?php echo $form->textField($model,'date_task',array('class'=>'none', 'value'=>$data)); ?>
	
	</div>

	<div class="block1 reg-form">
					<h3>Название Вашего проекта</h3>
	</div>
					
	<div class="row">

		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50, 'class'=>'auth-inp', 'placeholder'=>'Введите название')); ?>
		<?php echo $form->error($model,'title'); ?>
			<div class="block1 reg-form">Название должно содержать не более 50 символов	</div>

	</div>
	

	<div class="block1 reg-form">
					<h3>Бюджет проекта</h3>
	</div>	

<table style="margin:auto; width:440px;">
<tr><td>
	
		<?php echo $form->textField($model,'price',array('size'=>21,'maxlength'=>20, 'class'=>'auth-inp', 'placeholder'=>'Укажите сумму')); ?>
	
</td><td>
	
		<?php echo $form->textField($model,'pay_type',array('size'=>10,'maxlength'=>20, 'class'=>'auth-inp', 'placeholder'=>'рублей')); ?>
	
</td><td>

		<?php echo $form->textField($model,'cash_type',array('size'=>10,'maxlength'=>20, 'class'=>'auth-inp', 'placeholder'=>'за проект')); ?>
	
</td></tr>
</table>

	
	<!---
	
	<div class="row">
		<?php echo $form->labelEx($model,'spec'); ?>
		<?php echo $form->textField($model,'spec'); ?>
		<?php echo $form->error($model,'spec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profy'); ?>
		<?php echo $form->textField($model,'profy'); ?>
		<?php echo $form->error($model,'profy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'verify'); ?>
		<?php echo $form->textField($model,'verify'); ?>
		<?php echo $form->error($model,'verify'); ?>
	</div>
	-->
	
	
	<div class="row">

		<?php echo $form->textArea($model,'info',array('rows'=>3, 'cols'=>48, 'class'=>'auth-inp', 'placeholder'=>'Опишите подробно задание для исполнителя')); ?>

	</div>

<!--
<div class="block1">
					<h3>Выберите способ оплаты:</h3>
	</div>		
	<div class="row">
	<input type=radio>Безопасная оплата с резервированием средств<br><br>
	Безопасное сотрудничество с гарантией возврата средств.<br>
	Вы резервируете бюджет заказа на сайте - а мы гарантируем Вам<br>
	возврат суммы, если работа будет выполнена Исполнителем<br>
	некачественно или не в срок.
	</div>	



	<div class="row">
		<?php echo $form->labelEx($model,'pay_method'); ?>
		<?php echo $form->textField($model,'pay_method'); ?>
		<?php echo $form->error($model,'pay_method'); ?>
	</div>
	-->
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Опубликовать' : 'Save', array( 'class'=>'in-button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->