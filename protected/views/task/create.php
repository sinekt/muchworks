<?php
/* @var $this TaskController */
/* @var $model Task */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Task', 'url'=>array('index')),
	array('label'=>'Manage Task', 'url'=>array('admin')),
);
?>

<div class="row top-links">
                <div class="col-md-12">
                    <p class="text-center">Публикация проекта</p>
                </div>
</div>	

<?php $this->renderPartial('_form', array('model'=>$model)); ?>