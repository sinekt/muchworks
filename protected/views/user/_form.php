<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">

<table style="margin:auto;">
					
<tr><td>


		<?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>128, 'class'=>'auth-inp', 'placeholder'=>'E-mail')); ?>
		<?php echo $form->error($model,'email'); ?>

</td><td style="padding-left:20px; padding-right:20px;">
		
		<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>128, 'class'=>'auth-inp', 'placeholder'=>'Логин')); ?>
		<?php echo $form->error($model,'username'); ?>

</td><td>		
		
		<?php echo $form->passwordField($model,'password',array('size'=>20,'maxlength'=>128, 'class'=>'auth-inp', 'placeholder'=>'Пароль')); ?>
		<?php echo $form->error($model,'password'); ?>

		
</td></tr>


					<tr><td style="width:180px; text-align:left; font-size:11px; vertical-align:top;">
					Ваш электронный ящик
					</td><td style="padding-left:20px; padding-right:20px; width:180px; text-align:left; font-size:11px; vertical-align:top;">
					3-15 символов: латинские буквы, цифры, знак подчеркивания (_) и дефис (-)
					</td><td style="width:180px; text-align:left; font-size:11px; vertical-align:top;">
					От 6 до 24 символов. Допустимы латинские буквы, цифры и знак подчеркивания(_)
					</td></tr>
					

					
</table>		
		
		
		
	</div>

	
	
	

 <div class="row" style="margin-top:40px;">
<table style="width:450px; margin:auto;"><tr><td style="width:180px;text-align:left;">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/design/robot.png">
</td><td style="vertical-align:top; text-align:left;">
 <p style="color:white; font-size:16px; font-weight:300; margin-bottom:0;margin-top:10px;">Докажите, что Вы не робот!</p>
<p style="font-size:12px;">Введите код с картинки:</p>
 <div id="capcha">
 
 

       

<?php //echo CHtml::activeTextField($model,'verifyCode', array('placeholder'=>'Введите код','name'=>'verifyCode')); ?>



    
    
 
  <?php $this->widget('CCaptcha', array('showRefreshButton'=>false, 'clickableImage'=>true)); ?>
  <img src="<?php echo Yii::app()->request->baseUrl; ?>/design/cap-str.png" style="float:left; position:relative; left:12px; top:10px;"> 
  <input type="text" class="auth-inp" style="width:100px;">
 </div>
 
 
  
 <a href="/index.php/user/captcha/refresh/1" class="cap-link" style="text-decoration:underline;" target="new">Обновить картинку</a>
 </td></tr></table>
 
 </div>
	
	
	
	
	
	
 <div class="row otstup" style="margin-top:50px;">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Зарегистрироваться' : 'Save', array( 'class'=>'in-button')); ?>
 <div style="font-size:11px; display:inline-block; text-align:left; padding-left:20px; position:relative; top:5px;">Регистрируясь, Вы подтверждаете,<br>что согласны с <a href="#" class="restore" style="text-decoration:underline; color:#acacac;">правилами сервиса</a>
 </div>
 </div>

<?php $this->endWidget(); ?>

</div><!-- form -->