<?php
/* @var $this ProfileController */
/* @var $model profile */

$this->breadcrumbs=array(
	'Profiles'=>array('index'),
	$model->name=>array('view','id'=>$model->card_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List profile', 'url'=>array('index')),
	array('label'=>'Create profile', 'url'=>array('create')),
	array('label'=>'View profile', 'url'=>array('view', 'id'=>$model->card_id)),
	array('label'=>'Manage profile', 'url'=>array('admin')),
);
?>

<h1>Update profile <?php echo $model->card_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>