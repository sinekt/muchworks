<?php
/* @var $this ProfileController */
/* @var $model profile */
/* @var $form CActiveForm */
?>

<script>

var che;	

function countChecked() {
	che='';
	k=0;
$( ':checkbox:checked' ).each(function(){
	k++;
    che=che+' '+this.value;
    
});
return k;      
    }
    
function checker(x)
{

vseg=countChecked();

	if ($('#'+x).attr("checked")) 
	{
	$('#'+x).attr('checked', false);
	$('#'+x+'-l').attr('class', 'gray pointer');
	
	}
	else
	{
		
	
    if (vseg<3)	{
    
	$('#'+x).attr('checked', true);
	$('#'+x+'-l').attr('class', 'green pointer');}
	}
	
	
	
	
	
	countChecked();

	$('#chew').attr('value',che);
	

}
	
	function wchk(ar)
	{

		if (document.getElementById(ar).className=='whitecheck')
		{
		document.getElementById(ar).className='whitechecked';
		document.getElementById('showemail').value='0';
		}
		
		else
		{
		document.getElementById(ar).className='whitecheck';
		document.getElementById('showemail').value='1';
		}
	}
	
	
 function lico(lic)
 {
 	if (lic==1) 
 	{
 	tex='&nbsp;<a href="#" onclick="lico(0)">Частное лицо</a>&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Компания';
 	document.getElementById('licoo').innerHTML=tex;	
 	document.getElementById('licoval').value='0';
 		}
 	else 
 	{
	tex='Частное лицо<a href="#" onclick="lico(1)">&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Компания</a>';
 	document.getElementById('licoo').innerHTML=tex;
 	document.getElementById('licoval').value='1';
 	}
 }
	
</script>

<style>

#checki td {font-size:13px;}

</style>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>



<?php echo $form->errorSummary($model); ?>


<div class="row top-links2">
 
 <div class="mini-line"></div>
 
<span style="font-weight:300; color:#acacac; font-size:12px;">    

   
Кто Вы:
</span>
 <p class="text-center" style="display:inline;" id="licoo">
 	Частное лицо<a href="#" onclick="lico(1)">&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Компания</a>
 	</p>
 <br><br>          
</div>
<?php echo $form->textField($model,'lico',array('size'=>1,'id'=>'licoval', 'value'=>'0','class'=>'none')); ?>


		<?php 
		$uid=Yii::app()->user->id;
		echo $form->textField($model,'user_id',array('value'=>$uid,'class'=>'none')); ?>

<div class="row reg-form">
                
                    

					<table style="margin:auto;">
					
					<tr><td>
					<?php echo $form->textField($model,'family',array('class'=>'auth-inp', 'placeholder'=>'Фамилия')); ?>
					</td><td style="padding-left:20px; padding-right:20px;">
					<?php echo $form->textField($model,'name',array('class'=>'auth-inp', 'placeholder'=>'Имя')); ?>
					</td></tr>

				
					</table>
					<?php echo $form->textArea($model,'site',array('class'=>'osebe', 'placeholder'=>'Здесь Вы можете написать о себе')); ?>

					
<div class="block1">
					<h3>Что Вы умеете?</h3>
					Выберете до 3 специализаций, в которых хотите работать
					</div>
					
                
</div>
	

<br><br>



	<?php echo $form->textField($model,'special',array('id'=>'chew','class'=>'none')); ?>
	
	
<div  id="checki" class="block1" style="border-right:1px solid #2e9977; border-left:1px solid #2e9977;">
<table><tr><td style="padding-left:10px; width:210px;">
<input type="checkbox" id="ch1" class="none" value='1'/> <label id="ch1-l" onclick="checker('ch1');" class="gray pointer">Дизайн</label><br>
<input type="checkbox" id="ch2" class="none" value='2'/> <label id="ch2-l" onclick="checker('ch2');" class="gray pointer">Полиграфия</label><br>
<input type="checkbox" id="ch3" class="none" value='3'/> <label id="ch3-l" onclick="checker('ch3');" class="gray pointer">3D Графика</label><br>
<input type="checkbox" id="ch4" class="none" value='4'/> <label id="ch4-l" onclick="checker('ch4');" class="gray pointer">Копирайтинг, рерайтинг, SEO</label><br>
<input type="checkbox" id="ch5" class="none" value='5'/> <label id="ch5-l" onclick="checker('ch5');" class="gray pointer">Оптимизация SEO</label><br>
<input type="checkbox" id="ch6" class="none" value='6'/> <label id="ch6-l" onclick="checker('ch6');" class="gray pointer">Разработка сайтов</label><br>
<input type="checkbox" id="ch7" class="none" value='7'/> <label id="ch7-l" onclick="checker('ch7');" class="gray pointer">Разработка игр</label>
</td><td  style="">
<input type="checkbox" id="ch8" class="none" value='8'/> <label id="ch8-l" onclick="checker('ch8');" class="gray pointer">Расшифровка аудио</label><br>
<input type="checkbox" id="ch9" class="none" value='9'/> <label id="ch9-l" onclick="checker('ch9');" class="gray pointer">Телемаркетинг</label><br>
<input type="checkbox" id="ch10" class="none" value='10'/> <label id="ch10-l" onclick="checker('ch10');" class="gray pointer">Переводы</label><br>
<input type="checkbox" id="ch11" class="none" value='11'/> <label id="ch11-l" onclick="checker('ch11');" class="gray pointer">Анимация, мультипликация</label><br>
<input type="checkbox" id="ch12" class="none" value='12'/> <label id="ch12-l" onclick="checker('ch12');" class="gray pointer">Менеджмент</label><br>
<input type="checkbox" id="ch13" class="none" value='13'/> <label id="ch13-l" onclick="checker('ch13');" class="gray pointer">Консалтинг</label><br>
<input type="checkbox" id="ch14" class="none" value='14'/> <label id="ch14-l" onclick="checker('ch14');" class="gray pointer">Набор текста</label>
</td></tr></table>
</div>
 
 
<div class="row reg-form">  
<div class="block1">
					<h3>Ваше местоположение:</h3>
	
					</div>
					
                
</div> </div> 
 
 
					<table style="margin:auto;">
					
					<tr><td>
					<?php echo $form->textField($model,'country',array('class'=>'auth-inp', 'placeholder'=>'Страна')); ?>
					</td><td style="padding-left:20px; padding-right:20px;">
					<?php echo $form->textField($model,'city',array('class'=>'auth-inp', 'placeholder'=>'Город')); ?>
					</td></tr>

				
					</table>
 
 
 
 
 
<table style="margin:auto; width:300px;" class="reg-form">
<tr><td style="text-align:right;">
<h3>Добавьте фотографию:</h3>	
формат фото: jpg, jpeg, png<br>
Размер загружаемого изображения<br>
не более 5 мб<br>
<?php echo $form->fileField($model,'avatar',array('style'=>'float:right; width:190px;')); ?>
</td>
<td style="padding-left:20px;">
	<img src="/design/nophoto.gif">
</td></tr></table>
 



<div class="row reg-form">  
<div class="block1">
					<h3>Дополнительные контакты:</h3>
	
					</div>
					
                
</div> </div> 
 
 
					<table style="margin:auto;">
					
					<tr><td>
					<?php echo $form->textField($model,'phone',array('class'=>'auth-inp', 'placeholder'=>'Телефон')); ?>
					</td><td style="padding-left:20px; padding-right:20px;">
					<?php echo $form->textField($model,'skype',array('class'=>'auth-inp', 'placeholder'=>'Skype')); ?>
					</td></tr>

				
					</table>



 <div class="block1">
 	<div class="whitecheck" id="w1" onclick="wchk('w1');"></div>

 			<?php echo $form->textField($model,'showemail',array('id'=>'showemail','value'=>1, 'class'=>'none')); ?>
 Показывать мой профиль в списке исполнителей<br>
 Если хотите чтобы работодатели могли Вас найти
 </div>
	
<br><br>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить профиль', array( 'class'=>'in-button')); ?>
	</div>

<?php $this->endWidget(); ?>
<br><br>
</div><!-- form -->