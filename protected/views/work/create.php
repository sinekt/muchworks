<?php
/* @var $this WorkController */
/* @var $model Work */
$this->layout = '//layouts/black';
?>

<h1>Create Work</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>