		<style>
		.clean {padding-top:5%; }
		</style>

<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div class="row top-links">
                <div class="col-md-12">
                    <p class="text-center">Регистрация&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp; <a href="/index.php/site/login/">Вход на сайт</a></p>
                </div>
</div>		


<?php $this->renderPartial('_form', array('model'=>$model)); ?>