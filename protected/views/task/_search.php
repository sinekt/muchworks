<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'task_id'); ?>
		<?php echo $form->textField($model,'task_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_creator'); ?>
		<?php echo $form->textField($model,'id_creator'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_task'); ?>
		<?php echo $form->textField($model,'date_task'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'info'); ?>
		<?php echo $form->textArea($model,'info',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pay_type'); ?>
		<?php echo $form->textField($model,'pay_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cash_type'); ?>
		<?php echo $form->textField($model,'cash_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'spec'); ?>
		<?php echo $form->textField($model,'spec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profy'); ?>
		<?php echo $form->textField($model,'profy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'verify'); ?>
		<?php echo $form->textField($model,'verify'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pay_method'); ?>
		<?php echo $form->textField($model,'pay_method'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->