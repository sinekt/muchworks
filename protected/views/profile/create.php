<?php
/* @var $this ProfileController */
/* @var $model profile */

$this->breadcrumbs=array(
	'Profiles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List profile', 'url'=>array('index')),
	array('label'=>'Manage profile', 'url'=>array('admin')),
);
?>

		<style>
		
		@import "http://webfonts.ru/import/roboto.css";
		.clean {height:auto; padding-top:3%;}
		</style>

<div class="row top-links">
                <div class="col-md-12">
                    <p class="text-center">Редактирование профиля</p>
                </div>
</div>		


<?php $this->renderPartial('_form', array('model'=>$model)); ?>