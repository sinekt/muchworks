<?php
/* @var $this ProfileController */
/* @var $data profile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('card_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->card_id), array('view', 'id'=>$data->card_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lico')); ?>:</b>
	<?php echo CHtml::encode($data->lico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('family')); ?>:</b>
	<?php echo CHtml::encode($data->family); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avatar')); ?>:</b>
	<?php echo CHtml::encode($data->avatar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('skype')); ?>:</b>
	<?php echo CHtml::encode($data->skype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site')); ?>:</b>
	<?php echo CHtml::encode($data->site); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('special')); ?>:</b>
	<?php echo CHtml::encode($data->special); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showemail')); ?>:</b>
	<?php echo CHtml::encode($data->showemail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	*/ ?>

</div>