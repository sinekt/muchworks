$(document).ready(function() {
    // Initialise the first and second carousel by class selector.
    // Note that they use both the same configuration options (none in this case).
    $('.d-carousel .carousel').jcarousel({
        scroll: 1,
        wrap: 'circular'
    });
});