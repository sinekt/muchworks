<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
<link rel="icon" href="/favicon.ico" type="image/x-icon">			
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MuchWork</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/bootstrap.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/bootstrap-theme.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/style.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/puritan.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/main.css">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/design/css.css" rel="stylesheet" type="text/css">
		
		<style>
		
		@import "http://webfonts.ru/import/roboto.css";
		
		</style>
		
    </head>
<body>

       <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
		
		
		 <div id="navbar" class="navbar-collapse collapse">
		 
		 <div id="moy-cabinet" onclick="$('#moy-cabinet').animate({marginTop:'-400'},500);">
		 <br><br><br><br><br><br><br><br>
		 <font style="color:gray; position:relative; font-size:30px;">Здесь будет личный кабинет</font>
		 
		 </div>
		 
           <ul class="nav navbar-nav  pull-right">
               <li>
                   <a href="#" onclick="$('#moy-cabinet').animate({marginTop:'0'},500);">Мои проекты</a>
               </li>
               <li>
                   <a href="#" onclick="$('#moy-cabinet').animate({marginTop:'0'},500);">Мои Вакансии</a>
               </li>
               <li>
                   <a href="#" onclick="$('#moy-cabinet').animate({marginTop:'0'},500);">Мои Услуги</a>
               </li>
               <li>
                   <a href="#" onclick="$('#moy-cabinet').animate({marginTop:'0'},500);"><span class="color-green">+1</span> Заказы</a>
               </li>
               <li>
                   <a href="#" onclick="$('#moy-cabinet').animate({marginTop:'0'},500);"><span class="color-green">+2</span> Сообщения</a>
               </li>
               <li>
                   <a href="#" onclick="$('#moy-cabinet').animate({marginTop:'0'},500);">Личный кабинет</a>
               </li>
           </ul>

           

		   		<?php $this->widget('zii.widgets.CMenu',array('htmlOptions'=>array('class'=>'nav navbar-nav  pull-right'),
			'items'=>array(
				/*array('label'=>'Мои проекты', 'url'=>array('/site/index')),
				array('label'=>'Мои вакансии', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),*/
				array('label'=>'Войти', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		
           
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
	
	
	
	    <!-- Дополнительное меню -->
    <section id="second-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="/" style="margin:0;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/design/logo.png" alt="" class="img-responsive"></a>
                 
					
					<?php $this->widget('zii.widgets.CMenu',array('htmlOptions'=>array('class'=>'col-md-9 col-md-offset-3'),
			'items'=>array(
				array('label'=>'Проекты', 'url'=>array('/task')),
				array('label'=>'Вакансии', 'url'=>array('/work')),
				array('label'=>'Исполнители', 'url'=>array('/profile')),
				array('label'=>'Услуги', 'url'=>array('/work', 'view'=>'about')),				
				/*
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Войти', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)*/
			),
		)); ?>
					
                 
                </div>
            </div>
        </div>                
    </section>
	
	
    <section id="service">
        <div class="container">	
	

	<?php echo $content; ?>

	
        </div>
    </section> 


<div class="ots"></div>

      <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/design/footer-logo.png" alt="" class="img-responsive pull-left">
                    <a href="#">Справка</a> / <a href="#">Написать нам</a>
                    <p>© <?php echo date('Y'); ?> MuchWork.ru
                        <a href="#" class="pull-right">Узнать новости <span class="glyphicon glyphicon-arrow-right-w"></span></a>
                    </p> 
                    
                </div>
            </div>
        </div>        
      </footer><!-- /container -->  
	  


        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/jquery-1.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/modernizr-2.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/bootstrap.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/jquery.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/main.js"></script>
		
		
		
</body>
</html>
