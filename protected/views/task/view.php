<?php
/* @var $this TaskController */
/* @var $model Task */
$this->layout = '//layouts/main';
$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Task', 'url'=>array('index')),
	array('label'=>'Create Task', 'url'=>array('create')),
	array('label'=>'Update Task', 'url'=>array('update', 'id'=>$model->task_id)),
	array('label'=>'Delete Task', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->task_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Task', 'url'=>array('admin')),
);
?>

   <section id="project">
        <div class="container">
		
		
<div class="col-md-12">
                   <div class="project border-top-green">
                       
						<span class="color-red pull-right"><?php echo $model->price; ?> ₽ / <?php echo $model->pay_type; ?></span>
 
                        <h4><?php echo $model->title; ?></h4>
						
						
						<?php echo $model->info; ?>
						
						
                        <p class="color-grey">
						
						<?php $profy=$model->profy; if($profy=='1') { ?>
                            <span class="only">Только для
                                <img src="/design/profi.png" alt="" class="img-responsive">
                            </span>
						<?php } ?>
							
						<?php $verify=$model->verify; if($verify=='1') { ?>	
                            <span class="only">Только для
                                <span class="glyphicon glyphicon-user"></span>    
                            </span>
						<?php } ?>
						
						<?php $cash_type=$model->cash_type; if($cash_type=='1') { ?>	
                            <span class="only">Безопасная сделка
                                <span class="glyphicon glyphicon-check"></span>    
                            </span>
						<?php } ?>
                        </p>
                            <hr>
                            <span class="price">
							                       <p>
                            <span class="color-grey">Добавил: </span>
                            <span class="color-green"><?php $creator_id=$model->id_creator; $creator=User::model()->findByPk($creator_id); echo $creator->username; ?></span>
                            <span class="color-grey"><?php echo $task->date_task; ?></span>
                        </p>
						
                                                                
                            </span>          
                                     
                    </div>
                </div>
				
				
				
<div class="col-md-12">
                   <div class="addcomment">
				   
				   <input type=text><a href="#" class="combutton">+ Добавить</a>
				   
                    </div>
                </div>				
	</div>
    </section>
<?php /* $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'task_id',
		'id_creator',
		'date_task',
		'title',
		'info',
		'price',
		'pay_type',
		'cash_type',
		'spec',
		'profy',
		'verify',
		'pay_method',
	),
)); */?>
