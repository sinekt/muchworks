<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">	
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MuchWork</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/bootstrap.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/bootstrap-theme.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/style.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/puritan.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/design/main.css">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/design/css.css" rel="stylesheet" type="text/css">
		
		<style>
		
		@import "http://webfonts.ru/import/roboto.css";
		
		</style>
		
    </head>

    <body class="login">
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
 <div class="clean"> 
 <a href="/">
   <img src="<?php echo Yii::app()->request->baseUrl; ?>/design/logo.png" alt="" class="img-responsive" style="margin:auto; padding-bottom:10px;">
</a>


 <div class="row">
                <div class="col-md-12">
		
                    <p class="text-center" style="color:#808080;"> <a href="/" style="color:#808080;"><span class="glyphicon glyphicon-arrow-left"></span>Перейти на сайт</a></p>

                </div>
</div>
  
	
	
    <section id="service">
        <div class="container">	
	

	<?php echo $content; ?>

	
        </div>
    </section> 




 


        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/jquery-1.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/modernizr-2.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/bootstrap.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/jquery.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/design/main.js"></script>
		
		
		</div>
</body>
</html>
