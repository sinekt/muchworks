<?php
/* @var $this TaskController */
/* @var $data Task */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->task_id), array('view', 'id'=>$data->task_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_creator')); ?>:</b>
	<?php echo CHtml::encode($data->id_creator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_task')); ?>:</b>
	<?php echo CHtml::encode($data->date_task); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info')); ?>:</b>
	<?php echo CHtml::encode($data->info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pay_type')); ?>:</b>
	<?php echo CHtml::encode($data->pay_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cash_type')); ?>:</b>
	<?php echo CHtml::encode($data->cash_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spec')); ?>:</b>
	<?php echo CHtml::encode($data->spec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profy')); ?>:</b>
	<?php echo CHtml::encode($data->profy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('verify')); ?>:</b>
	<?php echo CHtml::encode($data->verify); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pay_method')); ?>:</b>
	<?php echo CHtml::encode($data->pay_method); ?>
	<br />

	*/ ?>

</div>